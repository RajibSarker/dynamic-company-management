import {Company, ExtendCompany} from "./company";

export class User{
  id: number;
  userName: string;
  mobileNo: string;
  address: string;
  userCompanies: Company[];
  extendedCompanies: ExtendCompany[];
}
