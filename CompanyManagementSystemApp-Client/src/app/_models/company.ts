import {User} from "./user";

export class Company{
  id: number;
  name: string;
  address: string;
  userId: number;
  user: User;
  values: any[];
}

export class ExtendCompany{
  userId: number;
  user: User;
  key: string;
  extendedCompanyValues: ExtendedCompanyValueDto[];
}

export class ExtendedCompanyValueDto{
  id: number;
  keyId: number;
  key: ExtendCompany;
  value: string;
}
