import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./components/login/login.component";
import {ConfigureCompanyComponent} from "./components/configure-company/configure-company.component";
import {SetupCompanyComponent} from "./components/setup-company/setup-company.component";
import {CompanyListComponent} from "./components/company-list/company-list.component";

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent, pathMatch: 'full'},
  {path: 'configure', component: ConfigureCompanyComponent, pathMatch: 'full'},
  {path: 'create', component: SetupCompanyComponent, pathMatch: 'full'},
  {path: 'update/:id', component: SetupCompanyComponent, pathMatch: 'full'},
  {path: 'list', component: CompanyListComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
