import {Component, OnInit} from '@angular/core';
import {CompanyService} from "../../services/company.service";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Company} from "../../_models/company";

@Component({
  selector: 'app-setup-company',
  templateUrl: './setup-company.component.html',
  styleUrls: ['./setup-company.component.css']
})
export class SetupCompanyComponent implements OnInit{

  userId: number;
  companyForm: FormGroup;
  userConfigurations: any[];
  existingCompanyId = 0;
  existingCompany: Company;
  constructor(
    private companyService: CompanyService,
    private _fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.companyService.isLoggedIn();

    this.companyService.user$.subscribe(user=>{
      this.userId = user.id;
    })
  }

  ngOnInit() {
    this.existingCompanyId = this.route.snapshot.params.id;

    this.companyForm = this._createCompanyForm();

    // get configurations
    this.getConfigurations();

    if(this.existingCompanyId){
      this.getExistingCompany();
    }
  }

  getExistingCompany(){
    if(this.existingCompanyId){
      this.companyService.getById(this.existingCompanyId).subscribe((res: any)=>{
        this.existingCompany = res;
        console.log(this.existingCompany);
        this.bindCompanyData();
      }, error=>{
        console.log(error);
      })
    }
  }

  private bindCompanyData() {
    if(this.existingCompany){
      this.companyForm.get('id').setValue(this.existingCompany.id);
      this.companyForm.get('name').setValue(this.existingCompany.name);
      this.companyForm.get('address').setValue(this.existingCompany.address);
      if (this.existingCompany && this.existingCompany.values && this.existingCompany.values.length > 0) {
        this.existingCompany.values.forEach((value, index) => {
          const i = this.getValuesForm().value.findIndex(c => c.keyId === value?.keyId);
          if (i >= 0) {
            let data = this.getValuesForm().at(i);
            if (data) {
              data.patchValue({value: value.value});
            }
          }
        })
      }
    }
  }

  private getConfigurations() {
    this.companyService.getUserConfigurations(this.userId).subscribe((res: any) => {
      this.userConfigurations = res;
      console.log(this.userConfigurations);

      if (this.userConfigurations && this.userConfigurations.length > 0) {
        this.companyForm.setControl('values', this.createConfiguration(this.userConfigurations));
      }

      // bind data
      this.bindCompanyData();
    }, error => {
      console.log(error);
    })
  }

  createConfiguration(data: any[]){
    let configurations = new FormArray([]);

    data.forEach(c=>{
      configurations.push(this._fb.group({
        keyId: c.id,
        key: c.key,
        value: ''
      }))
    })

    return configurations;
  }
  _createCompanyForm(){
    return this._fb.group({
      id: [0],
      userId: [this.userId],
      name: ['', Validators.required],
      address: [''],
      values: this._fb.array([])
    })
  }

  _createValuesForm(){
    return this._fb.group({
      keyId: [0],
      key: [''],
      value: ['']
    })
  }


  getValuesForm(){
    return this.companyForm.get('values') as FormArray;
  }

  onSubmit(){
    console.log(this.companyForm.getRawValue());

    if(this.companyForm.invalid){
      alert('Please fill correct value!');
      return;
    }



    if(this.existingCompanyId > 0){
      this.companyService.updateCompany(this.existingCompanyId, this.companyForm.getRawValue()).subscribe(res=>{
        if(res){
          alert('Update operation successful.');
          this.router.navigate(['list']);
        } else{
          alert('Update operation failed!');
        }
      }, error=>{
        console.log(error);
      })
    } else{
      this.companyService.addCompany(this.companyForm.getRawValue()).subscribe(res=>{
        if(res){
          alert('Company created successfully.');
          this.router.navigate(['list']);
        } else{
          alert('Company create operation failed!');
        }
      }, error=>{
        console.log(error);
      })
    }
  }
}
