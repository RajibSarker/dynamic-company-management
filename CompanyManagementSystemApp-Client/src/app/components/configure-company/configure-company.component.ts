import {Component, OnInit} from '@angular/core';
import {CompanyService} from "../../services/company.service";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-configure-company',
  templateUrl: './configure-company.component.html',
  styleUrls: ['./configure-company.component.css']
})
export class ConfigureCompanyComponent implements OnInit{

  userId = 0;
  extendForm: FormGroup;
  constructor(
    private companyService: CompanyService,
    private _fb: FormBuilder,
    private router: Router
  ) {
    this.companyService.isLoggedIn();

    this.companyService.user$.subscribe(user=>{
      this.userId = user.id;
    })
  }

  ngOnInit() {
    this.extendForm = this._fb.group({
      userId: this.userId,
      keys: this._fb.array([this.createKeyArray()])
    })
  }

  createKeyArray(){
    return this._fb.group({
      id: [0],
      key: ['', Validators.required]
    })
  }

  getKeysArray(){
    return this.extendForm.get('keys') as FormArray;
  }

  onSubmit(){
    if(this.extendForm.invalid){
      alert('Please input keys!');
      return;
    }

    this.companyService.configure(this.extendForm.getRawValue()).subscribe(res=>{
      if(res){
        alert('Configure operation successful.');
        this.router.navigate(['list']);
      } else{
        alert('Configure operation failed!');
      }
    }, error=>{
      console.log(error);
    })
  }

  onAddNewKey(){
    this.getKeysArray().push(this.createKeyArray());
  }
}
