import {Component, OnInit} from '@angular/core';
import {CompanyService} from "../../services/company.service";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit{
userId = 0;

  constructor(private companyService: CompanyService) {
    this.companyService.user$.subscribe(res=>{
      this.userId = res.id;
    })
  }

  ngOnInit() {
  }

  onLogout(){
    this.userId = 0;
    this.companyService.userId = 0;
    this.companyService.user$.next(null);
    this.companyService.isLoggedIn();
  }
}
