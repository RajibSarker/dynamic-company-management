import {Component, OnInit} from '@angular/core';
import {CompanyService} from "../../services/company.service";
import {Company} from "../../_models/company";

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit{

  userId = 0;
  companies: Company[] = [];
  extendedColumns: any[] = [];
  constructor(
    private companyService: CompanyService,
  ) {
    this.companyService.isLoggedIn();

    this.companyService.user$.subscribe(user=>{
      this.userId = user?.id;
    })
  }

  ngOnInit() {
    this.getExtendedColums();
    this.getUserCompanies();
  }

  getExtendedColums(){
    this.companyService.getUserConfigurations(this.userId).subscribe((res: any)=>{
        this.extendedColumns = res;
    }, error=>{
      console.log(error);
    })
  }

  onDelete(id: number){
    if(id){
      const isConfirmed = window.confirm('Are you sure you want to delete this company?');

      if (isConfirmed) {
        this.companyService.deleteCompany(id).subscribe(res=>{
          if(res){
            alert('Delete operation successful.');
            this.getUserCompanies();
          } else{
            alert('Delete operation failed!');
          }
        }, error=>{
          console.log(error);
        })
      }
    } else{
      alert('Company not found to delete!');
    }
  }

  getUserCompanies(){
    if(this.userId){
      this.companyService.list(this.userId).subscribe((res: any)=>{
        console.log(res);
        this.companies = res;
        // if(res && res.length > 0){
        //   res.forEach(company=>{
        //     if(company && company?.values && company.values?.length > 0){
        //       company.values.forEach(value=>{
        //         if(!this.extendedColumns.includes(value?.key?.key)){
        //           this.extendedColumns.push(value?.key?.key);
        //         }
        //       })
        //     }
        //     // delete company.id;
        //     // delete company.user;
        //     // delete company.values;
        //     // delete company.userId;
        //     this.companies.push(company);
        //   })
        // }
        //
        // console.log('Companies: ', this.companies);

      }, error=>{
        console.log(error);
      })
    }
  }
}
