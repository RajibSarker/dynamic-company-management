import {Component, OnInit} from '@angular/core';
import {CompanyService} from "../../services/company.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit{

  userName: string;
  userId: string;
  constructor(
  private companyService: CompanyService,
  private router: Router
  ) {
  }

  ngOnInit() {
  }

  onSubmit(){
    if(this.userName === null || this.userName === undefined || this.userName === '') {
      alert('Please enter user name!');
    }

    const user = {userName: this.userName};

    this.companyService.login(user).subscribe((res: any)=>{
      if(res){
        this.companyService.userId = res.id;
        this.companyService.user$.next(res);
        this.userId = res.userId;
        alert('Login successful.');
        this.router.navigate(['list']);
      } else{
        alert('Login failed! Please try again later.');
      }
    }, error=>{
      console.log(error);
    })
  }
}
