import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";

const httpOptions: any = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Methods': 'GET',
    // 'Access-Control-Allow-Origin': '*'
    // "Access-Control-Allow-Origin": "*",
    // "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
  })
};

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  baseUrl = 'https://localhost:7004/api/companies';
  userId = 0;
  user$ = new BehaviorSubject<any>(0);

  constructor(private http: HttpClient, private router: Router) { }

  login(user){
    return this.http.post( this.baseUrl + '/login', user, {headers: httpOptions.headers});
  }

  configure(dataToConfigure){
    return this.http.post(this.baseUrl + '/extendCompany', dataToConfigure, {headers: httpOptions.headers});
  }

  list(userId: number){
    return this.http.get(this.baseUrl + '/'+ userId + '/user');
  }

  addCompany(data){
    return this.http.post(this.baseUrl, data);
  }

  updateCompany(id: number, data){
    return this.http.put(this.baseUrl + '?id='+ id, data, {headers: httpOptions.headers} );
  }

  deleteCompany(id){
    return this.http.delete(this.baseUrl + '/'+ id, {headers: httpOptions.headers});
  }

  getUserConfigurations(userId: number){
    return this.http.get(this.baseUrl + '/getUserConfigurations/' + userId, {headers: httpOptions.headers} );
  }

  getById(id: number){
    return this.http.get(this.baseUrl+'/' + id, {headers: httpOptions.headers});
  }

  isLoggedIn(){
    if(this.userId > 0){
      return true;
    } else{
      this.router.navigate(['login']);
    }
  }

}
