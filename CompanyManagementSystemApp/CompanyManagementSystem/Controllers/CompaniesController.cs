﻿using AutoMapper;
using CompanyManagementSystem.Managers;
using CompanyManagementSystem.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CompanyManagementSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly ICompanyManager _manager;
        private readonly IMapper _mapper;

        public CompaniesController(ICompanyManager manager, IMapper mapper)
        {
            _manager = manager;
            _mapper = mapper;
        }

        [HttpGet("{userId}/user")]
        public async Task<IActionResult> GetAll(long userId)
        {
            return Ok(await _manager.GetAllCompaniesAsync(userId));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            if (id == 0) return BadRequest("Invalid input request.");

            return Ok(await _manager.GetCompanyById(id));
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CompanyForCreationDto inputRequest)
        {
            if (inputRequest == null) return BadRequest("Invalid input request.");

            return Ok(await _manager.AddCompanyAsync(inputRequest));
        }

        [HttpPut]
        public async Task<IActionResult> Put(long id, [FromBody] CompanyForUpdateDto inputRequest)
        {
            if (inputRequest == null) return BadRequest("Invalid input request.");

            return Ok(await _manager.UpdateCompanyAsync(inputRequest));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(long id)
        {
            if (id == 0) return BadRequest("Invalid input request.");

            return Ok(await _manager.DeleteAsync(id));
        }

        [HttpPost("extendCompany")]
        public async Task<IActionResult> ExtendCompany([FromBody] ExtendedCompanyForCreationDto inputRequest)
        {
            if (inputRequest == null)
            {
                return BadRequest("Invalid input request.");
            }

            return Ok(await _manager.ExtendCompany(inputRequest));
        }

        [HttpPost("addNewUser")]
        public async Task<IActionResult> AddNewUser([FromBody] UserForCreationDto user)
        {
            var dataToSave = _mapper.Map<User>(user);
            return Ok(await _manager.AddNewUser(dataToSave));
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] UserToLogin login)
        {
            return Ok(await _manager.Login(login.UserName));
        }

        [HttpGet("getUserConfigurations/{userId}")]
        public async Task<IActionResult> GetUserConfigurations(long userId)
        {
            return Ok(await _manager.GetUserConfigurations(userId));
        }
    }
}
