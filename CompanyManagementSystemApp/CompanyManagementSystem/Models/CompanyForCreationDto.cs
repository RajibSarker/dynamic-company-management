﻿namespace CompanyManagementSystem.Models
{
    public class CompanyForCreationDto
    {
        public CompanyForCreationDto()
        {
            Values = new List<ExtendedCompanyValueForCreationDto>();
        }

        public long UserId { get; set; }
        public string Name { get;set; }
        public string Address { get; set; }
        public ICollection<ExtendedCompanyValueForCreationDto> Values { get; set; }
    }

}
