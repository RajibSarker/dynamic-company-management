﻿namespace CompanyManagementSystem.Models
{
    public class UserForCreationDto
    {
        public string UserName { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }
    }
}
