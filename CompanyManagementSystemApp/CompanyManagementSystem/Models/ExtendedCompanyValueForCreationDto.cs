﻿namespace CompanyManagementSystem.Models
{
    public class ExtendedCompanyValueForCreationDto
    {
        public long KeyId { get; set; }
        public string Value { get; set; }
    }
}
