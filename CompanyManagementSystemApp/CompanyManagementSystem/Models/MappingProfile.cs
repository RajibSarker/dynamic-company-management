﻿using AutoMapper;

namespace CompanyManagementSystem.Models
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<CompanyForCreationDto, Company>().ReverseMap();
            CreateMap<CompanyForUpdateDto, Company>().ReverseMap();
            CreateMap<ExtendedCompanyForCreationDto, ExtendedCompany>();
            CreateMap<ExtendedCompanyValueForCreationDto, ExtendedCompanyValue>();
            CreateMap<UserForCreationDto, User>();
        }
    }
}
