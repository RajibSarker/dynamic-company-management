﻿namespace CompanyManagementSystem.Models
{
    public class ExtendedCompanyForCreationDto
    {
        public ExtendedCompanyForCreationDto()
        {
            Keys = new List<ExtendedKey>();
        }
        public long UserId { get; set; }
        public ICollection<ExtendedKey> Keys { get; set; }
    }

    public class ExtendedKey
    {
        public string Key { get; set; }
    }
}
