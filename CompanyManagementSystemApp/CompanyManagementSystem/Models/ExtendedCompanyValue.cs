﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CompanyManagementSystem.Models
{
    [Table("ExtendedCompanyValues")]
    public class ExtendedCompanyValue
    {
        public long Id { get; set; }
        public long CompanyId { get; set; }
        public Company Company { get; set; }
        public long KeyId { get; set; }
        public ExtendedCompany Key { get; set; }
        public string Value { get; set; }
    }
}
