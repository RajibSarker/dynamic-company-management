﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Principal;

namespace CompanyManagementSystem.Models
{
    [Table("Users")]
    public class User
    {
        public User()
        {
            UserCompanies = new List<Company>();
            ExtendedCompanies = new List<ExtendedCompany>();
        }
        public long Id { get; set; }
        public string UserName { get; set; }
        public string MobileNo { get; set; }
        public string Address { get; set; }

        public ICollection<Company> UserCompanies { get; set; }
        public ICollection<ExtendedCompany> ExtendedCompanies { get; set; }
    }
}
