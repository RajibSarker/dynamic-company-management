﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CompanyManagementSystem.Models
{
    [Table("ExtendedCompanies")]
    public class ExtendedCompany
    {
        public ExtendedCompany()
        {
            ExtendedCompanyValues = new List<ExtendedCompanyValue>();
        }
        public long Id { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public string Key { get; set; }

        public ICollection<ExtendedCompanyValue> ExtendedCompanyValues { get; set; }
    }
}
