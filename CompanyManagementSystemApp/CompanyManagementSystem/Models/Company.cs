﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CompanyManagementSystem.Models
{
    [Table("Companies")]
    public class Company
    {
        public Company()
        {
            Values = new List<ExtendedCompanyValue>();
        }
        public long Id { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        public ICollection<ExtendedCompanyValue> Values { get; set; }
    }
}
