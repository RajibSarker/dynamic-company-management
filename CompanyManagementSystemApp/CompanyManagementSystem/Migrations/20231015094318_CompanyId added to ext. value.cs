﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CompanyManagementSystem.Migrations
{
    public partial class CompanyIdaddedtoextvalue : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "CompanyId",
                table: "ExtendedCompanyValues",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_ExtendedCompanyValues_CompanyId",
                table: "ExtendedCompanyValues",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_ExtendedCompanyValues_Companies_CompanyId",
                table: "ExtendedCompanyValues",
                column: "CompanyId",
                principalTable: "Companies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ExtendedCompanyValues_Companies_CompanyId",
                table: "ExtendedCompanyValues");

            migrationBuilder.DropIndex(
                name: "IX_ExtendedCompanyValues_CompanyId",
                table: "ExtendedCompanyValues");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "ExtendedCompanyValues");
        }
    }
}
