﻿using CompanyManagementSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace CompanyManagementSystem.Database
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Company> Companies { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ExtendedCompany> ExtendedCompanies { get; set; }
        public DbSet<ExtendedCompanyValue> ExtendedCompanyValues { get; set; }
    }
}
