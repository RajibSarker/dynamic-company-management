﻿using AutoMapper;
using CompanyManagementSystem.Database;
using CompanyManagementSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace CompanyManagementSystem.Repositories
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly ApplicationDbContext _db;
        private readonly IMapper _mapper;

        public CompanyRepository(ApplicationDbContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }
        public async Task<bool> AddCompanyAsync(CompanyForCreationDto inputRequest)
        {
            if (inputRequest == null) return false;

            var dataToSave = _mapper.Map<Company>(inputRequest);

            await _db.Companies.AddAsync(dataToSave);
            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<bool> UpdateCompanyAsync(CompanyForUpdateDto inputRequest)
        {
            if(inputRequest == null) return false;

            var existingCompany = await GetCompanyById(inputRequest.Id);
            if(existingCompany == null) return false;

            var dataToUpdate = _mapper.Map(inputRequest, existingCompany);

            _db.Entry(existingCompany).State = EntityState.Modified;
            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<bool> DeleteAsync(long id)
        {
            if (id == 0) return false;

            var existingCompany = await GetCompanyById(id);
            if(existingCompany == null) return false;

            _db.Companies.Remove(existingCompany);
            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<ICollection<Company>> GetAllCompaniesAsync(long userId)
        {
            return await _db.Companies.Include(c=>c.Values).
                ThenInclude(c=>c.Key).Where(c=>c.UserId == userId).ToListAsync();
        }

        public async Task<Company> GetCompanyById(long id)
        {
            if(id == 0) return null;

            return await _db.Companies.Include(c => c.Values).FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<bool> ExtendCompany(ExtendedCompanyForCreationDto inputRequest)
        {
            if (inputRequest == null) return false;

            List<ExtendedCompany> dataList = new List<ExtendedCompany>();
            foreach (var key in inputRequest.Keys)
            {
                ExtendedCompany ext = new ExtendedCompany
                {
                    UserId = inputRequest.UserId,
                    Key = key.Key
                };

                dataList.Add(ext);
            }

            await _db.ExtendedCompanies.AddRangeAsync(dataList);
            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<bool> AddNewUser(User inputRequest)
        {
            await _db.Users.AddAsync(inputRequest);
            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<User> Login(string userName)
        {
            if (string.IsNullOrEmpty(userName)) return null;

            return await _db.Users.FirstOrDefaultAsync(c => c.UserName.ToLower() == userName.ToLower());
        }

        public async Task<ICollection<ExtendedCompany>> GetUserConfigurations(long userId)
        {
            if (userId == 0) return null;

            return await _db.ExtendedCompanies.Include(c=>c.ExtendedCompanyValues).Where(c => c.UserId == userId)
                .ToListAsync();
        }
    }
}
