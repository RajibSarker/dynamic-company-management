﻿using CompanyManagementSystem.Models;
using CompanyManagementSystem.Repositories;

namespace CompanyManagementSystem.Managers
{
    public class CompanyManager: ICompanyManager
    {
        private readonly ICompanyRepository _repository;

        public CompanyManager(ICompanyRepository repository)
        {
            _repository = repository;
        }
        public async Task<bool> AddCompanyAsync(CompanyForCreationDto inputRequest)
        {
            return await _repository.AddCompanyAsync(inputRequest);
        }

        public async Task<bool> UpdateCompanyAsync(CompanyForUpdateDto inputRequest)
        {
            return await _repository.UpdateCompanyAsync(inputRequest);
        }

        public async Task<bool> DeleteAsync(long id)
        {
            return await _repository.DeleteAsync(id);
        }

        public async Task<ICollection<Company>> GetAllCompaniesAsync(long userId)
        {
            return await _repository.GetAllCompaniesAsync(userId);
        }

        public async Task<Company> GetCompanyById(long id)
        {
            return await _repository.GetCompanyById(id);
        }

        public async Task<bool> ExtendCompany(ExtendedCompanyForCreationDto inputRequest)
        {
            return await _repository.ExtendCompany(inputRequest);
        }

        public async Task<bool> AddNewUser(User inputRequest)
        {
            return await _repository.AddNewUser(inputRequest);
        }

        public async Task<User> Login(string userName)
        {
            return await _repository.Login(userName);
        }

        public async Task<ICollection<ExtendedCompany>> GetUserConfigurations(long userId)
        {
            return await _repository.GetUserConfigurations(userId);
        }
    }
}
