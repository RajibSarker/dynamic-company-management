﻿using CompanyManagementSystem.Models;

namespace CompanyManagementSystem.Managers
{
    public interface ICompanyManager
    {
        Task<bool> AddCompanyAsync(CompanyForCreationDto inputRequest);
        Task<bool> UpdateCompanyAsync(CompanyForUpdateDto inputRequest);
        Task<bool> DeleteAsync(long id);
        Task<ICollection<Company>> GetAllCompaniesAsync(long userId);
        Task<Company> GetCompanyById(long id);
        Task<bool> ExtendCompany(ExtendedCompanyForCreationDto inputRequest);
        Task<bool> AddNewUser(User inputRequest);
        Task<User> Login(string userName);
        Task<ICollection<ExtendedCompany>> GetUserConfigurations(long userId);
    }
}
